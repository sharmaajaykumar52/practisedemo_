package com.journaldev.spring.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.journaldev.spring.Utility.Helper;

@Controller
public class HomeController {

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
	
		Helper.displayMethodExecutionName(locale, "Home");
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage(Locale locale, Model model) {
		Helper.displayMethodExecutionName(locale, "login");
		return "userLogin";
	}

	@RequestMapping(value = "/userLogin", method = RequestMethod.POST)
	public String doLogin(Locale locale, Model model, @ModelAttribute UserLoginDTO userLoginDTO) {
		System.out
				.println("User Name Is " + userLoginDTO.getUserName() + " Password " + userLoginDTO.getUserPassword());
		Helper.displayMethodExecutionName(locale, "userLogin");
		return "userLogin";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegisterationPage(Locale locale, Model model) {
		Helper.displayMethodExecutionName(locale, "Register");
		return "register";
	}
	
	@RequestMapping(value = "/registerUser", method = RequestMethod.GET)
	public String showRegisterationPageInReact(Locale locale, Model model) {
		Helper.displayMethodExecutionName(locale, "RegisterUser");
		return "registerUser";
	}
}
