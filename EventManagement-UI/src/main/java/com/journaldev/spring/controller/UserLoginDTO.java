package com.journaldev.spring.controller;

import java.io.Serializable;

public class UserLoginDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7612873650868580376L;

	private String userName;
	private String userPassword;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

}
