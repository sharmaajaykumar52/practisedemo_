package com.journaldev.spring.Utility;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Helper {

	public static void displayMethodExecutionName(Locale locale, String methodName) {
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		System.out.println(methodName + " Page Requested, locale = " + locale + "on time " + formattedDate);

	}

}
