<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="theme-color" content="#000000" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title>React App</title>
<style>
.bg-light {
	background-color: beige !important;
}

#loginPageDiv {
	margin-top: 6%;
}
</style>
</head>
<body>
	<noscript>You need to enable JavaScript to run this app.</noscript>

	<nav class="navbar navbar-expand-sm bg-light navbar-light">
		<a class="navbar-brand" href="#"> <img
			src="https://www.bmo.com/resources/images/logos/bank-of-montreal/bmo.svg"
			alt="bmo-logo" style="width: 100%">
		</a>
	</nav>

	<div id="loginPageDiv"></div>
	<link rel="stylesheet"
		href="<c:url value="/resources/css/loginpage.css" />">
	<script src="<c:url value="/resources/js/loginPage.js" />"></script>
</body>
</html>

