<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <%@ page
session="false"%>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

	<!-- CDN Changes For DatePicker-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
</head>
<title>Registration</title>
<style>
	.switch-field {
		display: flex;
		margin-bottom: 7px;

	}

	.switch-field input {
		position: absolute !important;
		clip: rect(0, 0, 0, 0);
		height: 1px;
		width: 1px;
		border: 0;
		overflow: hidden;
	}

	.switch-field label {
		background-color: #e4e4e4;
		color: rgba(0, 0, 0, 0.6);
		font-size: 14px;
		line-height: 1;
		text-align: center;
		padding: 8px 14px;
		margin-right: -1px;
		border: 1px solid rgba(0, 0, 0, 0.2);
		box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
		transition: all 0.5s ease-in-out;
	}

	.switch-field label:hover {
		cursor: pointer;
	}

	.switch-field input:checked+label {
		background-color: #483d8b;
		box-shadow: none;
		color: white;
	}

	.switch-field label:first-of-type {
		border-radius: 4px 0 0 4px;
	}

	.switch-field label:last-of-type {
		border-radius: 0 4px 4px 0;
	}

	/* This is just for CodePen. */

	.form {
		max-width: 600px;
		font-family: "Lucida Grande", Tahoma, Verdana, sans-serif;
		font-weight: normal;
		line-height: 1.625;
		margin: 8px auto;
		padding: 16px;
	}

	h2 {
		font-size: 18px;
		margin-bottom: 8px;
	}

	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	.switch input {
		opacity: 0;
		width: 0;
		height: 0;
	}

	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked+.slider {
		background-color: #483d8b
	}

	input:focus+.slider {
		box-shadow: 0 0 1px #483d8b
	}

	input:checked+.slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;
	}

	.slider.round:before {
		border-radius: 50%;
	}

	#aadharCardFile,
	#panCardCardFile {
		background-color: #a9a9a94f;
	}

	.progress {
		background-color: #a9a9a94f;
		margin-right: 6%;
		margin-left: 1%;
	}

	.progress-bar {

		background-color: #a9a9a94f;
		color: black
	}

	.change-progress-bar-color {
		background-color: #483d8b;
		color: whitesmoke;
	}

	#loginButton {
		margin-top: 3%;
	}

	html,
	body {
		height: 100%;
	}

	.outer {
		position: relative;
	}

	.inner {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}

	#paddingInDiv {
		padding-top: 10px;
	}

	.navbar-default .navbar-toggle:focus,
	.navbar-default .navbar-toggle {
		background-color: #ddd;
		margin-right: 10%;
	}

	body {
		background-color: gainsboro;
	}

	.navbar-default {
		background-color: #483d8b;
		border-color: #483d8b;
	}

	.blockSizeDiv {
		background-color: antiquewhite;
	}

	.boxShadow {
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	}

	#myNavbar>ul {
		padding-top: 1%;
	}

	.navbar-default .navbar-nav>li>a {
		color: whitesmoke
	}

	#centeredText>span {
		text-align: center;
		font-size: x-large
	}

	@media (min-width : 300px) and (max-width: 640px) {
		.div-padding {
			margin-top: 15%;
		}

		#centeredText {
			margin-left: -6%;
			margin-bottom: 10%;
		}

		#loginButton {
			margin-top: 10%;
		}
	}

	/*  Medium devices (tablets, 768px and up) */
	@media (min-width : 641px) and (max-width: 768px) {
		.div-padding {
			margin-top: 15%;
		}

		#centeredText {
			margin-right: 8%;
			padding: 0%;
		}

		#loginButton {
			margin-top: 3%;
		}
	}

	/*  Large devices (desktops, 992px and up) */
	@media (min-width :769px)and (max-width: 992px) {
		.div-padding {
			margin-top: 8%;
		}

		#centeredText {
			margin-right: 8%;
			padding: 0%;
		}

		#loginButton {
			margin-top: 3%;
		}
	}

	/*    // Extra large devices (large desktops, 1200px and up) */
	@media (min-width :993px) and (max-width: 1300px) {
		.div-padding {
			margin-top: 15%;
		}

		#centeredText {
			margin-right: 8%;
			padding: 0%;
		}

		#loginButton {
			margin-top: 3%;
		}
	}

	@media (min-width : 1301px) and (max-width:1500px) {
		.div-padding {
			margin-top: 10%;
		}

		#centeredText {
			margin-right: 8%;
			padding: 0%;
		}

		#loginButton {
			margin-top: 3%;
		}
	}
</style>

<body>
	<!-- Navigation Bar  -->
	<div class="container-fluid row">
		<nav class="navbar navbar-default navbar-fixed-top boxShadow">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#myPage">Logo</a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#">Home</a></li>
						<li><a href="#events">Events</a></li>
						<li><a href="#upComingEvents">Up Coming Events</a></li>
						<li><a href="#contactUs">Contact Us</a></li>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#"><span class="glyphicon glyphicon-user"></span>
									Sign Up</a></li>
							<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>
									Login</a></li>
						</ul>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div class="container-fluid div-padding">
		<div class="col-xs-12 col-sm-12 col-md-1"></div>
		<div class="col-xs-12 col-sm-12 col-md-10">
			<div class="col-sm-12 col-xs-12 col-md-12 boxShadow"
				style="background-color: #a9a9a94f; padding-top: 5%; padding-bottom: 5%; border: 15px #a9a9a94f solid">
				<!-- Progress Bar-->
				<div class="row">
					<div class="progress">
						<div class="progress-bar progress-bar-success" role="progressbar" id="basicRegdProgBar"
							style="width:33.33%">
							<b>Basic Registeration</b>
						</div>
						<div class="progress-bar progress-bar-success" role="progressbar" id="contactFormProgBar"
							style="width:33.334%">
							<b>Contact Information</b></div>
						<div class="progress-bar progress-bar-success" role="progressbar" id="AddrInfoProgBar"
							style="width:33.334%">
							<b>Address Information</b>
						</div>
					</div>
					<form name="loginModel" class="form-horizontal" id="loginModel">

						<!-- Enter Basic Details-->
						<div class="col-sm-12" id="basicRegisteration">

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="organizerName">Organizer Name</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control inValid" id="organizerName"
										placeholder="Enter Organizer Name" name="organizerName">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">

								<div class="col-sm-6 form-group">
									<label class="control-label" for="isMarried">Are you Married</label>
								</div>
								<div class="col-sm-6">
									<div class="switch-field">
										<input type="radio" id="isMarriedY" class='inValid' name="isMarried"
											value="Y" />
										<label for="isMarriedY">Yes</label>
										<input type="radio" id="isMarriedN" class='inValid' name="isMarried" value="N"
											checked />
										<label for="isMarriedN">No</label>
									</div>
								</div>
							</div>



							<div class="col-sm-12 col-xs-12 col-md-6">

								<div class="col-sm-6 form-group">
									<label class="control-label" for="panCardCardFile">PanCard Upload File</label>
								</div>
								<div class="col-sm-6">
									<input type="file" class="form-control inValid" id="panCardCardFile"
										placeholder="Upload PanCard" name="panCardCardFile">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="panCardNumber">Pan Card</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control inValid" id="panCardNumber"
										placeholder="Enter PanCard" name="panCardNumber">
								</div>
							</div>



							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="anniverseryDate">Anniversery Date</label>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div class='input-group date' id='anniverseryDate'>
											<input type='text' class="form-control inValid" name="anniverseryDate"
												id="anniverseryDateText" placeholder="Anniversery Date DD-MM-YYYY" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="dateOfBirth">Date Of Birth</label>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div class='input-group date' id='dateOfBirth'>
											<input type='text' class="form-control inValid" name="dateOfBirth"
												id="dateOfBirthText" placeholder="Date of birth DD-MM-YYYY" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-12 col-md-6">

								<div class="col-sm-6 form-group">
									<label class="control-label" for="Gender">Gender</label>
								</div>
								<div class="col-sm-6">
									<div class="switch-field">
										<input type="radio" id="genderMale" class='inValid' name="gender" value="male"
											checked />
										<label for="genderMale">Male</label>
										<input type="radio" id="genderFeMale" name="gender" class='inValid'
											value="female" />
										<label for="genderFeMale">Female</label>
										<input type="radio" id="genderOther" name="gender" class='inValid'
											value="Other" />
										<label for="genderOther">Other</label>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="aadharCardFile">Aadhar Card File</label>
								</div>
								<div class="col-sm-6">
									<input type="file" class="form-control inValid" id="aadharCardFile"
										name="aadharCardFile">
								</div>
							</div>
							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="aadharCardNumber">Aadhar Card Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control inValid" id="aadharCardNumber"
										placeholder="Enter Aadhar Card" name="aadharCardNumber">
								</div>
							</div>


							<div class="col-sm12">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<ul class="pager">
										<li class="previous"><a href="#"
												style="background-color: #483d8b;color: white;cursor: none;">Previous</a>
										</li>
										<li class="next"><a href="#" id="hidebasicRegisterationDetail"
												style="background-color: #483d8b;color: white">Next</a>
										</li>
									</ul>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>

						<!-- Enter Contact Details-->
						<div class="col-sm-12" id="contactDetail">
							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="emailId">Email Id</label>
								</div>
								<div class="col-sm-6">
									<input type="email" class="form-control" id="emailId"
										placeholder="Enter Valid Email Id" name="emailId">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="emailId">Alternative Email Id</label>
								</div>
								<div class="col-sm-6">
									<input type="email" class="form-control" id="emailId"
										placeholder="Enter Alt. Email Id" name="emailId">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="mobileNumber">Mobile Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="mobileNumber"
										placeholder="Enter Mobile Number" name="mobileNumber">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="emailId">Alternative Mobile Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="altMobileNumber"
										placeholder="Enter Alt. Mobile Number" name="altMobileNumber">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="landLineNumber">Landline Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="landLineNumber"
										placeholder="Enter Landline Number" name="landLineNumber">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="faxMachineNumber">Fax Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="faxMachineNumber"
										placeholder="Enter Fax Number" name="faxMachineNumber">
								</div>
							</div>

							<div class="col-sm12">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<ul class="pager">
										<li class="previous"><a href="#" id="showBasiRegisterationDetail"
												style="background-color: #483d8b;color: white">Previous</a></li>
										<li class="next"><a href="#" id='hideContactDetail' "
												style=" background-color: #483d8b;color: white">Next</a>
										</li>
									</ul>
								</div>
								<div class="col-sm-1"></div>
							</div>
						</div>

						<!-- Enter Address Details-->
						<div class="col-sm-12" id="addressDetail">
							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="houseNumber">House Number</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="houseNumber"
										placeholder="Enter House Number" name="houseNumber">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="locality">Locality</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="locality" placeholder="Enter Locality"
										name="locality">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="state">State</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="state" placeholder="Enter State"
										name="state">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="city">City</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="city" placeholder="Enter City"
										name="city">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="country">Country</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="country" placeholder="Enter Country"
										name="country">
								</div>
							</div>

							<div class="col-sm-12 col-xs-12 col-md-6">
								<div class="col-sm-6 form-group">
									<label class="control-label" for="pincode">Pincode</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="pincode" placeholder="Enter Pincode"
										name="pincode">
								</div>
							</div>



							<div class="col-sm12">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<ul class="pager">
										<li class="previous"><a href="#" id="hideAddressDetail"
												style="background-color: #483d8b;color: white">Previous</a></li>
										<li class="next"><a href="#"
												style="background-color: #483d8b;color: white;cursor: none;">Next</a>
										</li>
									</ul>
								</div>
								<div class="col-sm-1"></div>

								<!-- Submit Button -->
								<div class="col-sm-12 col-xs-12" id="loginButton">
									<div class="col-sm-4"></div>
									<div class="col-sm-3">
										<div class="btn-group btn-group-justified">
											<a href="#" class="btn btn-primary" style="background-color: #483d8b"
												onclick="return submitorganizerDetails()">Submit</a>
										</div>
									</div>
									<div class="col-sm-1"></div>
								</div>
							</div>
						</div>



					</form>
				</div>
			</div>
		</div>
		<div class="col-sm-2"></div>
	</div>

	<script>
		function submitorganizerDetails() {
			var loginFormData = {};
			var jsonRequest = "";
			$('#loginModel input, #loginModel select ,#loginModel textarea')
				.each(function (index) {
					var input = $(this);
					//alert('Type: ' + input.attr('type') + 'Name: ' + input.attr('name') + 'Value: ' + input.val());                   
					var inputName = input.attr('name');
					var inputValue = input.val()
					console.log('inputName ' + inputName + ' inputValue ' + inputValue + ' input.attr ' + input.attr('type') + ' input checked ' + input.attr('checked'));
					if ((input.attr('type') === 'checkbox' || input.attr('type') === 'radio') && input.attr('checked') === 'checked') {
						loginFormData[inputName] = inputValue;
					} else if ((input.attr('type') === 'checkbox' || input.attr('type') === 'radio') && input.attr('checked') != 'checked') {
						console.log('dummy Value');
					} else {
						loginFormData[inputName] = inputValue;
					}
				});
			console.log("jsonRequest  -------->  "
				+ JSON.stringify(loginFormData));

			$.ajax({
				type: "POST",
				headers: {
					'Access-Control-Allow-Origin': '*'
				},
				url: "http://172.16.22.103:4001/user/addDetails",
				cache: false,
				data: JSON.stringify(loginFormData),
				success: function () {
					console.log("data send successfully !!!!");
				},
				dataType: "json",
				contentType: "application/json"
			});

		}

		$(document).ready(function () {
			//By default only basicRegisteration will be shown other div having Id contactDetail and addressDetail will he hidden.
			$('#basicRegisteration').show();
			$('#contactDetail').hide();
			$('#addressDetail').hide();

			$('#dateOfBirth,#anniverseryDate').datetimepicker({ format: 'DD-MM-YYYY' });
			//$(".form-control").attr("style", "border: '-1px solid #ccc !important'");



			$('#basicRegisteration').on('change', function () {
				var basicRegisteration = $("#basicRegisteration :input[type='text'],#basicRegisteration :input[type='radio'],#basicRegisteration :input[type='file'],#dateOfBirthText,#anniverseryDateText");
				basicRegisteration.each(function (index) {
					var input = $(this);
					var value = input.val();
					var id = '#' + input.attr('id');
					if (value.length > 0) {
						$(id).addClass('valid');
						$(id).removeClass('inValid');
					} else {
						$(id).removeClass('valid');
						$(id).addClass('inValid');
					}
				});
				basicRegisteration.each(function (index) {
					var input = $(this);
					var value = input.val();
					var classAttribute = '#' + input.attr('class');
					var id = '#' + input.attr('id');
					if (classAttribute.indexOf('valid') > 0) {
						$('#basicRegdProgBar').addClass('change-progress-bar-color');
					}
					if (classAttribute.indexOf('inValid') >= 0) {
						$('#basicRegdProgBar').removeClass('change-progress-bar-color');
					}
				});
			});



			$('#addressDetail').on('change', function () {
				var addressDetail = $("#addressDetail :input[type='text'],#addressDetail :input[type='radio'],#addressDetail :input[type='file']");
				addressDetail.each(function (index) {
					var input = $(this);
					var value = input.val();
					var id = '#' + input.attr('id');
					if (value.length > 0) {
						$(id).addClass('valid');
						$(id).removeClass('inValid');
					} else {
						$(id).removeClass('valid');
						$(id).addClass('inValid');
					}
				});
				addressDetail.each(function (index) {
					var input = $(this);
					var value = input.val();
					var classAttribute = '#' + input.attr('class');
					var id = '#' + input.attr('id');
					if (classAttribute.indexOf('valid') > 0) {
						$('#AddrInfoProgBar').addClass('change-progress-bar-color');
					}
					if (classAttribute.indexOf('inValid') >= 0) {
						$('#AddrInfoProgBar').removeClass('change-progress-bar-color');
					}
				});
			});


			$('#contactDetail').on('change', function () {
				var contactDetail = $("#contactDetail :input[type='text'],#contactDetail :input[type='radio'],#contactDetail :input[type='file']");
				contactDetail.each(function (index) {
					var input = $(this);
					var value = input.val();
					var id = '#' + input.attr('id');
					if (value.length > 0) {
						$(id).addClass('valid');
						$(id).removeClass('inValid');
					} else {
						$(id).removeClass('valid');
						$(id).addClass('inValid');
					}
				});
				contactDetail.each(function (index) {
					var input = $(this);
					var value = input.val();
					var classAttribute = '#' + input.attr('class');
					var id = '#' + input.attr('id');
					if (classAttribute.indexOf('valid') > 0) {
						$('#contactFormProgBar').addClass('change-progress-bar-color');
					}
					if (classAttribute.indexOf('inValid') >= 0) {
						$('#contactFormProgBar').removeClass('change-progress-bar-color');
					}
				});
			});

			$('#hidebasicRegisterationDetail').on('click', function () {
				$('#basicRegisteration').hide();
				$('#contactDetail').show();
				$('#addressDetail').hide();
			});
			$('#hideContactDetail').on('click', function () {
				$('#basicRegisteration').hide();
				$('#contactDetail').hide();
				$('#addressDetail').show();
			});
			$('#showBasiRegisterationDetail').on('click', function () {
				$('#basicRegisteration').show();
				$('#contactDetail').hide();
				$('#addressDetail').hide();
			});
			$('#hideAddressDetail').on('click', function () {
				$('#basicRegisteration').hide();
				$('#contactDetail').show();
				$('#addressDetail').hide();
			});
		});
	</script>
</body>

</html>