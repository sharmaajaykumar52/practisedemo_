<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> <%@ page
session="false"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no"
	charset="utf-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
</head>
<title>Login</title>
<style>
#loginButton {
	margin-top: 3%;
}

html, body {
	height: 100%;
}

.outer {
	position: relative;
}

.inner {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}

#paddingInDiv {
	padding-top: 10px;
}

.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle {
	background-color: #ddd;
	margin-right: 10%;
}

body {
	background-color: gainsboro;
}

.navbar-default {
	background-color: #483d8b;
	border-color: #483d8b;
}

.blockSizeDiv {
	background-color: antiquewhite;
}

.boxShadow {
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0
		rgba(0, 0, 0, 0.19);
}

#myNavbar>ul {
	padding-top: 1%;
}

.navbar-default .navbar-nav>li>a {
	color: whitesmoke
}

#centeredText>span {
	text-align: center;
	font-size: x-large
}

@media ( min-width : 300px) and (max-width: 640px) {
	.div-padding {
		margin-top: 35%;
	}
	#centeredText {
		margin-left: -6%;
		margin-bottom: 10%;
	}
	#loginButton {
		margin-top: 10%;
	}
}

/*  Medium devices (tablets, 768px and up) */
@media ( min-width : 641px) and (max-width: 768px) {
	.div-padding {
		margin-top: 30%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
	#loginButton {
		margin-top: 3%;
	}
}

/*  Large devices (desktops, 992px and up) */
@media ( min-width :769px)and (max-width: 992px) {
	.div-padding {
		margin-top: 8%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
	#loginButton {
		margin-top: 3%;
	}
}

/*    // Extra large devices (large desktops, 1200px and up) */
@media ( min-width :993px) and (max-width: 1300px) {
	.div-padding {
		margin-top: 35%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
	#loginButton {
		margin-top: 3%;
	}
}

@media ( min-width : 1301px) and (max-width:1500px) {
	.div-padding {
		margin-top: 12%;
	}
	#centeredText {
		margin-right: 8%;
		padding: 0%;
	}
	#loginButton {
		margin-top: 3%;
	}
}
</style>

<body>
	<div class="container-fluid row">
		<nav class="navbar navbar-default navbar-fixed-top boxShadow">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#myPage">Logo</a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#">Home</a></li>
						<li><a href="#events">Events</a></li>
						<li><a href="#upComingEvents">Up Coming Events</a></li>
						<li><a href="#contactUs">Contact Us</a></li>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#"><span class="glyphicon glyphicon-user"></span>
									Sign Up</a></li>
							<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>
									Login</a></li>
						</ul>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	<div class="container-fluid div-padding">
		<div class="col-xs-12 col-sm-12 col-md-2"></div>
		<div class="col-xs-12 col-sm-12 col-md-7">
			<div class="col-sm-12 col-xs-12 col-md-12 boxShadow"
				style="background-color: #a9a9a94f; padding-top: 5%; padding-bottom: 5%; border: 15px #a9a9a94f solid">
				<!-- Enter Username-->
				<div class="row">
					<form name="loginModel" class="form-horizontal" id="loginModel">
						<div class="col-sm-12 col-xs-12">
							<div class="col-sm-2"></div>
							<div class="col-sm-4 form-group" style="text-align: center;">
								<label class="control-label" for="username">Username</label>
							</div>
							<div class="col-sm-5">
								<input type="text" class="form-control" id="userName"
									placeholder="Enter Username" name="username">
							</div>
						</div>


						<!-- Enter Password  -->
						<div class="col-sm-12 col-xs-12">
							<div class="col-sm-2"></div>
							<div class="col-sm-4 form-group" style="text-align: center;">
								<label class="control-label" for="password">Password</label>
							</div>
							<div class="col-sm-5">
								<input type="password" class="form-control" id="password"
									placeholder="Enter password" name="password">
							</div>
						</div>

						<!-- Submit Button -->
						<div class="col-sm-12 col-xs-12" id="loginButton">
							<div class="col-sm-4"></div>
							<div class="col-sm-3">
								<div class="btn-group btn-group-justified">
									<a href="#" class="btn btn-primary"
										style="background-color: #483d8b"
										onclick="return submitLoginDetails()">Login</a>
								</div>
							</div>
							<div class="col-sm-5"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-sm-2"></div>
	</div>

	<script>
		function submitLoginDetails() {
			var loginFormData = {};
			var jsonRequest = "";
			$('#loginModel input, #loginModel select ,#loginModel textarea')
					.each(function(index) {
						var input = $(this);
						//alert('Type: ' + input.attr('type') + 'Name: ' + input.attr('name') + 'Value: ' + input.val());                   
						var inputName = input.attr('name');
						var inputValue = input.val()
						loginFormData[inputName] = inputValue;
					});
			console.log("jsonRequest  -------->  "
					+ JSON.stringify(loginFormData));

			$.ajax({
				type : "POST",
				headers : {
					'Access-Control-Allow-Origin' : '*'
				},
				url : "http://172.16.22.103:4001/user/login",
				cache : false,
				data : JSON.stringify(loginFormData),
				success : function() {
					console.log("data send successfully !!!!");
				},
				dataType : "json",
				contentType : "application/json"
			});

		}
	</script>
</body>

</html>